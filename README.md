# Running FastAPI services in Docker

Tutorial on setting up and running a fairly minimal FastAPI service 
in a Docker, using the simple SpaCy entity linker from 
<http://pypi.org/project/spacy-entity-linker/> as example. 

I ran this on Linux Mint 20.1 Ulyssa and Docker version 19.03.

## Run the original example

Use example from <http://pypi.org/project/spacy-entity-linker/>.
(file 'original-example.py'). 

```buildoutcfg
pip install spacy
pip install spacy-entity-linker
python -m spacy download en_core_web_sm
python -m spacyEntityLinker download_knowledge_base
python original-example.py
```

## Run in a Docker

1. Choose a docker base image, for example Python:3.8.

Check it out:
```buildoutcfg
docker run -ti python:3.8 bash
```   

2. Install the necessary packages.
   
__Try it out inside the base image first!__

(From home, I could not do the pip installs while connected through VPN.)
```buildoutcfg
root@ea2eafeb3aab:/# pip install spacy
root@ea2eafeb3aab:/# pip install spacy-entity-linker
root@ea2eafeb3aab:/# python -m spacy download en_core_web_sm
root@ea2eafeb3aab:/# python -m spacyEntityLinker download_knowledge_base
```

This doesn't work (because our code is not inside the docker yet):
```buildoutcfg
root@ea2eafeb3aab:/# python original-example.py
```

But we can cut and paste from outside the docker to test:
```buildoutcfg
root@ea2eafeb3aab:/# cat > original-example.py

    ...cut-and-paste the Python code here...

^D
```

3. When it works we can write a Dockerfile:
```buildoutcfg
FROM python:3.8

ADD ./original_example.py /

RUN pip install spacy
RUN pip install spacy-entity-linker
RUN python -m spacy download en_core_web_sm
RUN python -m spacyEntityLinker download_knowledge_base

CMD ["python", "./original_example.py"]
```

4. Create image:
```buildoutcfg
docker image build -t spacy-entity-linker .
```

5. Run image as container:
```buildoutcfg
docker run spacy-entity-linker
```

## Run as FastAPI service outside Docker

Extend example from <http://pypi.org/project/spacy-entity-linker/>.
See file 'fast-api-example.py'.

```buildoutcfg
pip install fastapi
pip install uvicorn
python fast-api-example.py
```

The service now waits for input. In a web browser or Postman, go to: 
* http://0.0.0.0:8000
* http://0.0.0.0:8000/test
* http://0.0.0.0:8000/link?text="Stockholm, Sweden"

_Note: this simple example linker appears to be a priori-based, 
and does not use context to resolve disambiguities._

## Run FastAPI service in Docker

1. Install the necessary packages.
   
__Try it out inside the base image first!__

Start shell inside container:
```buildoutcfg
docker run -it spacy-entity-linker /bin/bash
```

(From home, I could not do the pip installs while connected through VPN.)
```buildoutcfg
root@ea2eafeb3aab:/# pip install fastapi
root@ea2eafeb3aab:/# pip install uvicorn
```

Cut and paste from outside the docker to test:
```buildoutcfg
root@ea2eafeb3aab:/# cat > fast-api-example.py

    ...cut-and-paste the new Python code here...

^D
```

```buildoutcfg
root@ea2eafeb3aab:/# python fast-api-example.py
```

It runs! But only inside the docker, so we cannot connect to it...

```buildoutcfg
^C
root@ea2eafeb3aab:/# exit
```

Start shell inside container:
```buildoutcfg
docker run -it -p 8000:8000 spacy-entity-linker /bin/bash
```

2. When it works we can write a Dockerfile-FastAPI:
```buildoutcfg
FROM python:3.8

ADD ./fast-api-example.py /

RUN python -m pip install --upgrade pip
RUN pip install spacy
RUN pip install spacy-entity-linker
RUN python -m spacy download en_core_web_sm
RUN python -m spacyEntityLinker download_knowledge_base
RUN pip install fastapi
RUN pip install uvicorn

CMD ["python", "./fast-api-example.py"]
```

3. Create image:
```buildoutcfg
docker image build -f Dockerfile-FastAPI -t spacy-nel-api .
```

4. Run image as container:
```buildoutcfg
docker run -d -p 8000:8000 spacy-nel-api
```

The dockerised service now waits for input. In a web browser or Postman, go to: 
* http://0.0.0.0:8000
* http://0.0.0.0:8000/test
* http://0.0.0.0:8000/link?text="Copenhagen, Denmark"

To run it on another internal host address and port: 
```
docker run -dt -p 127.0.0.127:127:8000 spacy-nel-api
```
