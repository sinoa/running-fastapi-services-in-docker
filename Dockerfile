FROM python:3.8

ADD ./original-example.py /

RUN pip install spacy
RUN pip install spacy-entity-linker
RUN python -m spacy download en_core_web_sm
RUN python -m spacyEntityLinker download_knowledge_base

CMD ["python", "./original-example.py"]
