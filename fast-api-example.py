# Based on example from https://pypi.org/project/spacy-entity-linker/

import spacy
from spacyEntityLinker import EntityLinker

# NEW:
from fastapi import FastAPI
import uvicorn

# initialize Entity Linker
entityLinker = EntityLinker()

# initialize language model
nlp = spacy.load("en_core_web_sm")

# add pipeline
nlp.add_pipe(entityLinker, last=True, name="entityLinker")

text = "They live in Cambridge, a city in Middlesex County, Massachusetts, in the U.S."
# text = "They live in Cambridge, a university city in Cambridgeshire, England, north of London."


# NEW: the linking is now in a function
def _nel(text):
    doc = nlp(text)

    # returns all entities in the whole document
    all_linked_entities = doc._.linkedEntities

    # NEW: the function must return JSON
    return [ {
                'text':     str(le.span),
                'url':      'https://www.wikidata.org/wiki/Q' + str(le.identifier),
                'label':    str(le.label),
    } for le in all_linked_entities ]


# NEW: set up FastAPI
# everything below is boilerplate, except the app name one the last line
app = FastAPI()


@app.get('/')
def home():
    return {'response': """
<h1>spaCy entity linker</h1>
<p>Test with .../test" .</p>
<p>Run with .../link?text="...text to lift..." .</p>

"""}


@app.post('/')
def home_post():
    return home()


@app.get('/link')
def link(text: str):
    if text:
        return _nel(text)
    else:
        return "Error: No text argument given."


@app.post('/link')
def link_post(json: dict):
    if json and 'text' in json:
        return link(json['text'])
    else:
        return "POST data must be 'application/json' with a 'text' entry."


@app.get('/test')
def test():
    return _nel(text)


@app.post('/test')
def test_post():
    return test()


# NEW:
if __name__ == '__main__':
    uvicorn.run('fast-api-example:app', host='0.0.0.0', port=8000)
