# Based on example from https://pypi.org/project/spacy-entity-linker/

import spacy
from spacyEntityLinker import EntityLinker


# initialize Entity Linker
entityLinker = EntityLinker()

# initialize language model
nlp = spacy.load("en_core_web_sm")

# add pipeline
nlp.add_pipe(entityLinker, last=True, name="entityLinker")

text = "They live in Cambridge, a city in Middlesex County, Massachusetts, in the U.S."
# text = "They live in Cambridge, a university city in Cambridgeshire, England, north of London."

doc = nlp(text)

# returns all entities in the whole document
all_linked_entities=doc._.linkedEntities

# iterates over sentences and prints linked entities
for sent in doc.sents:
    sent._.linkedEntities.pretty_print()
